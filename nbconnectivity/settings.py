""" Tropolink API configuration """

TROPOLINK_API_STUDIES = 'http://0.0.0.0:8000/studies'
TROPOLINK_API_TRAJECTORIES = 'http://0.0.0.0:8000/trajectories'
TROPOLINK_API_CONNECTIVITIES = 'http://0.0.0.0:8000/connectivities'
TROPOLINK_API_DOWNLOAD = 'http://0.0.0.0:8000/connectivities'

# Django OIDC endpoints
TROPOLINK_API_TOKEN = 'http://0.0.0.0:8000/token/'
TROPOLINK_API_REFRESH_TOKEN = 'http://0.0.0.0:8000/token/refresh/'

# Keycloak endpoint
TROPOLINK_AUTH_REFRESH_TOKEN = 'http://0.0.0.0:8000/auth/realms/tropolink/protocol/openid-connect/token/'
TROPOLINK_AUTH_CLIENT_ID = 'tropolink-api-front'

try:
    from .settings_local import *
except ImportError:
    pass
