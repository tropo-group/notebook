import datetime
import ipywidgets as widgets
import requests
import json
import jwt
import pandas as pd

from .settings import *
from .graph import *


class Importer(widgets.Box):

    def __init__(self, access_token=None, refresh_token=None):
        """
        Constructor
        """
        super(Importer, self).__init__()

        self.access_token = access_token
        self.refresh_token = refresh_token
        self.show_login = self.access_token is None and self.refresh_token is None

        # Initialize UI
        self._init_ui()

    def _login(self):
        self.debug(f'_login {TROPOLINK_API_TOKEN} {self.userarea.value}')
        try:
            response = requests.post(TROPOLINK_API_TOKEN, data={
                'username': self.userarea.value,
                'password': self.userpassword.value,
            })
            response.raise_for_status()
            data = response.json()
            self.access_token = data['access_token']
            self.refresh_token = data['refresh_token']
            self.debug(f'  logged in')
        except requests.exceptions.RequestException as e:
            self.debug(f'  _login error {e}')
            self.studies_errormsg.value = str(e)

    def _refresh(self):
        try:
            if self.show_login:
                self.debug(f'_refresh {TROPOLINK_API_REFRESH_TOKEN}')
                # We have a token from Django
                response = requests.post(TROPOLINK_API_REFRESH_TOKEN, data={
                    'refresh': self.refresh_token,
                })
                response.raise_for_status()
                data = response.json()
                self.access_token = data['access_token']
                self.refresh_token = data['refresh_token']
                self.debug('  refreshed')
            else:
                self.debug(f'_refresh {TROPOLINK_AUTH_REFRESH_TOKEN}')
                # We have a token from the front
                response = requests.post(TROPOLINK_AUTH_REFRESH_TOKEN, data={
                    'client_id': TROPOLINK_AUTH_CLIENT_ID,
                    'grant_type': 'refresh_token',
                    'refresh_token': self.refresh_token,
                })
                response.raise_for_status()
                data = response.json()
                self.access_token = data['access_token']
                self.refresh_token = data['refresh_token']
                self.debug('  refreshed')
        except requests.exceptions.RequestException as e:
            self.debug(f'  _refresh error {e}')
            self.studies_errormsg.value = str(e)

    def _is_access_token_expired(self, access_token):
        try:
            jwt.decode(access_token, algorithms="HS256", options={"verify_signature": False})
        except jwt.exceptions.ExpiredSignatureError:
            return True
        return False

    def get(self, url):
        self.debug(f'get {url}')
        # Log in if necessary
        if self.access_token is None and self.refresh_token is None:
            self._login()
        elif self.access_token is None or self._is_access_token_expired(self.access_token):
            self.access_token = None
            self._refresh()
        # Could not log in
        if self.access_token is None:
            self.debug('Not logged in, aborting get')
            return
        try:
            self.studies_errormsg.value = f'Loading {url}'
            r = requests.get(url, headers={'Authorization': f'Bearer {self.access_token}'})
            r.raise_for_status()
            self.studies_errormsg.value = f''
            self.debug(f'  got {url}')
            return r.json()
        except ConnectionError as err:
            # manage connection errors
            self.debug(f'  get connection error {err}')
            self.studies_errormsg.value = f'Unable to connect to {url} to get your studies.'

        except requests.exceptions.RequestException as err:
            # manage requests errors
            self.debug(f'  get error {err}')
            self.studies_errormsg.value = str(err)

        return None

    def debug(self, message):
        self.DEBUG.value = message + '\n' + self.DEBUG.value

    def _init_ui(self):
        """
        Initializes the UI
        """
        self.trajectory_id = None

        # Load the widgets' dictionnary
        with open('nbconnectivity/nbconnectivity.json') as json_data:
            self.widgetsDict = json.load(json_data)

        # Initialize widgets
        if self.show_login:
            self._init_username(self.widgetsDict["username"])
        self._init_trajectory(self.widgetsDict["trajectory"])
        self._init_connectivities(self.widgetsDict["connectivities"])
        self._init_studies(self.widgetsDict["study_name"])
        self._init_debug()

        if self.show_login:
            center = widgets.GridspecLayout(2, 3)
            center[0, 0] = self.userarea
            center[0, 1] = self.userpassword
            center[0, 2] = self.userbutton
            center[1, 0] = self.studies_dropdown
            center[1, 1:] = self.studies_errormsg
        else:
            center = widgets.GridspecLayout(1, 2)
            center[0, 0] = self.studies_dropdown
            center[0, 1] = self.studies_errormsg
            self._load_studies()

        self.UI = widgets.AppLayout(
            header=None,
            left_sidebar=None,
            center=center,
            right_sidebar=None,
            pane_heights=[1, '100px', '30px'],
        )

    def _init_debug(self):
        self.DEBUG = widgets.Textarea(
            value='',
            description='Debug:',
            disabled=False,
            layout=widgets.Layout(width='100%', height='200px')
        )

    def _init_username(self, widgets_dict):
        """
        Initializes a text area to enter the username
        :param widgetsDict: a dictionary with widgets strings
        """
        self.userbutton = widgets.Button(
            description=widgets_dict["button"]["label"],
            tooltip=widgets_dict["button"]["description"]
        )

        self.userarea = widgets.Text(
            placeholder=widgets_dict["userarea"]["placeholder"],
            description=widgets_dict["userarea"]["label"],
            description_tooltip=widgets_dict["userarea"]["description"]
        )

        self.userpassword = widgets.Password(
            placeholder=widgets_dict["password"]["placeholder"],
            description=widgets_dict["password"]["label"],
            description_tooltip=widgets_dict["password"]["description"]
        )

        self.userbutton.on_click(self._load_studies)
        self.userarea.observe(self._reinit_username, "value")

    def _init_studies(self, widgets_dict):
        """
        Initializes a dropdown menu to select the study
        :param widgetsDict: a dictionary with widgets strings
        """
        self.studies_dropdown = widgets.Dropdown(
            placeholder=widgets_dict["placeholder"],
            description=widgets_dict["label"],
            description_tooltip=widgets_dict["description"]
        )

        self.studies_errormsg = widgets.Label()

        self.studies_ui = widgets.HBox(
            children=[
                self.studies_dropdown,
                self.studies_errormsg
            ]
        )

        self.studies_dropdown.observe(self._get_study_content, "value")

    def _init_graph(self, widgets_dict):
        """
        Initializes a graph area
        :param widgets_dict: a dictionary with widgets strings
        """
        plotbutton_layout = widgets.Layout(
            height='42px',
            width="100px"
        )

        self.plotbutton = widgets.Button(
            description=widgets_dict["button"]["label"],
            tooltip=widgets_dict["button"]["description"],
            layout=plotbutton_layout,
            disabled=True
        )

        self.plotbutton.on_click(self._view_graph)

        self.minconnectivity = widgets.BoundedIntText(
            min=0,
            max=10,
            step=1,
            value=0,
            description=widgets_dict["options"]["minconnectivity"]["label"],
            style={'description_width': 'initial'},
        )

        self.plottype = widgets.RadioButtons(
            description=widgets_dict["options"]["plottype"]["label"],
            options=widgets_dict["options"]["plottype"]["values"]
        )

        plotoptions_content = widgets.VBox(
            children=[self.minconnectivity, self.plottype]
        )

        plotoptions_layout = widgets.Layout(
            height='150px',
            width="520px"
        )
        plotoptions = widgets.Accordion(
            children=[plotoptions_content],
            selected_index=None,
            layout=plotoptions_layout
        )
        plotoptions.set_title(0, widgets_dict["options"]["label"])

        self.plotwidgets = widgets.HBox(
            children=[self.plotbutton, plotoptions],
        )

    def _reinit_username(self, change):
        """
        Reinit UI when the username is changed
        :param change: a username is provided
        """
        self.userbutton.button_style = ''
        self.userbutton.icon = ''
        self.studies_errormsg.value = ''
        self.studies_dropdown.options = []

    def _init_trajectory(self, widgets_dict):
        """
        Initializes a dropdown menu to select the date of trajectories
        :param widgetsDict: a dictionary with widgets strings
        """
        self.date_dropdown = widgets.Dropdown(
            placeholder=widgets_dict["placeholder"],
            description=widgets_dict["label"],
            description_tooltip=widgets_dict["description"]
        )

        self.date_errormsg = widgets.Label()

        self.dates_ui = widgets.HBox(
            children=[
                self.date_dropdown,
                self.date_errormsg
            ]
        )

    def _init_connectivities(self, widgets_dict):
        """
        Initializes a dropdown menu to select the connectivity computation
        :param widgetsDict: a dictionary with widgets strings
        """
        self.connectivities_dropdown = widgets.Dropdown(
            placeholder=widgets_dict["placeholder"],
            description=widgets_dict["label"],
            description_tooltip=widgets_dict["description"]
        )

        self.connectivities_errormsg = widgets.Label()

        self.connectivities_ui = widgets.HBox(
            children=[
                self.connectivities_dropdown,
                self.connectivities_errormsg
            ]
        )

        self.connectivities_dropdown.observe(self._get_connectivity, "value")

    def _load_studies(self, change=None):
        """
        Load the list of previous studies names
        """
        self.debug('_load_studies')
        self.studies = []

        # Get the studies for a registered user from the Tropolink API
        self.studies = self.get(TROPOLINK_API_STUDIES)
        if self.studies is None:
            return

        # Create a list of study names
        studies_list = [
            (study["name"], study["id"])
            for study in self.studies
            if study["trajectory"]["status"] == "succeeded"
        ]

        if len(studies_list) == 0:
            self.studies_errormsg.value = "No study was found."
            if self.show_login:
                self.userbutton.icon = ''
                self.userbutton.button_style = 'warning'
        else:
            studies_list.sort()
            self.studies_dropdown.options = studies_list
            if self.show_login:
                self.userbutton.icon = 'check'
                self.userbutton.button_style = 'success'

    def _load_connectivities(self, change):
        """
        Load the list of connectivity
        """
        self.debug('_load_connectivities')
        self.connectivities = []

        # Get the studies for a registered user from the Tropolink API
        self.connectivities = self.get(TROPOLINK_API_CONNECTIVITIES)
        if self.connectivities is None:
            return

        # Create a list of connectivities names
        connectivities_list = []
        for connectivity in self.connectivities:
            if connectivity["status"] == "succeeded":
                connectivities_list.append(connectivity["name"])

        if len(connectivities_list) == 0:
            self.studies_errormsg.value = "No connectivities calculation was found."
            if self.show_login:
                self.userbutton.icon = ''
                self.userbutton.button_style = 'warning'
        else:
            connectivities_list.sort()
            self.studies_dropdown.options = connectivities_list
            if self.show_login:
                self.userbutton.icon = 'check'
                self.userbutton.button_style = 'success'

    def get_trajectory_geometry(self, date):
        """
        Get the geometry of each trajectory
        :param traj_id: Trajectory computation ID
        :return Geometries in a dictionary (easily convertable to a GeoDataFrame)
        """
        self.debug('get_trajectories_geometry')
        result = self.get(f'{TROPOLINK_API_TRAJECTORIES}/{self.trajectory_id}/get_geometry/{date}/')
        if result is None:
            return None
        return result

    def get_distances_matrix(self):
        """
        :param change: traj_id: Trajectory computation ID
        :return: the distances matrix as pandas array
        """
        self.debug('get_distances_matrix')
        # Get the distances for a registered user from the Tropolink API
        distances_json = self.get(f'{TROPOLINK_API_TRAJECTORIES}/{self.trajectory_id}/get_distances_matrix/')
        if distances_json is None:
            return None

        # Add labels on rows / columns
        distances_array = np.array(distances_json)
        distances_df = pd.DataFrame(distances_array,
                                    columns=self.locations['CODE'],
                                    index=self.locations['CODE'])
        return distances_df

    def get_connectivity_matrix(self, connectivity_name):
        """
        :param change: a connectivity name is provided
        :return: the connectivity matrix as pandas array
        """
        self.debug('get_connectivity_matrix')
        for item in self.connectivities:
            if item["name"] == connectivity_name:
                connectivity_id = item["id"]
                break

        # Get the studies for a registered user from the Tropolink API
        connectivity_json = self.get(f'{TROPOLINK_API_DOWNLOAD}/{connectivity_id}/get_connectivity_json/')
        if connectivity_json is None:
            return None

        # Add labels on rows / columns
        connectivity_df = pd.DataFrame(
            connectivity_json["connectivity"]["matrix"],
            columns=connectivity_json["connectivity"]["col_names"],
            index=connectivity_json["connectivity"]["row_names"],
        )
        return connectivity_df

    def get_study_detail(self, study_id):
        return self.get(f'{TROPOLINK_API_STUDIES}/{study_id}/')

    def _get_study_content(self, change):
        """
        Get trajectories computed by HYSPLIT
        :param change: a study id is provided
        """
        # Find connectivity id
        study_id = change["new"]
        self.debug(f'_get_study_content {study_id}')
        study = self.get_study_detail(study_id)
        if study is not None:
            self.trajectory_id = study["trajectory"]["id"]

            # Get locations in trajectory metadata
            locations_list = [loc.split() for loc in study["trajectory"]["locations"]]
            self.locations = pd.DataFrame(
                locations_list,
                columns=['CODE', 'NAME', 'LATITUDE', 'LONGITUDE', 'HEIGHT']
            )

            # Get distances
            self.distances_matrix = self.get_distances_matrix()
            if self.distances_matrix is None:
                return

            self.date_dropdown.options = list(study["trajectory"]["dates"])

            self.studies_errormsg.value = 'Study loaded'

            # Get connectivity computations
            self.connectivities = study["connectivities"]
            self.connectivities_dropdown.options = self._get_connectivities_names(self.connectivities)
        else:
            self.trajectories = {}
            self.locations = None
            self.connectivities = {}
            self.distances_matrix = None

    def _get_connectivities_names(self, connectivities_list):
        """
        Get a list of connectivity computation names
        :param connectivities_list: list of connectivity computations
        :return: a list
        """
        self.debug('_get_connectivities_names')
        connectivities_names = []
        for connectivity in connectivities_list:
            connectivities_names.append(connectivity["name"])
        return connectivities_names

    def _get_connectivity(self, change):
        """
        Get the connectivity matrix and the locations when the connectivity selected changes
        :param change: a connectivity name is provided
        """
        self.debug('_get_connectivity')
        # Find connectivity id
        connectivity_name = change["new"]

        if len(self.connectivities_dropdown.options) > 0 and connectivity_name:
            self.connectivity_matrix = self.get_connectivity_matrix(connectivity_name)
        else:
            self.connectivity_matrix = None

    def _view_graph(self, change):
        self.debug('_view_graph')

        if self.connectivity_matrix is None or self.connectivity_matrix.shape[1] == 0:
            self.studies_errormsg.value = 'No connectivity matrix'
            return

        # Get connectivity name
        connectivity_name = self.studies_dropdown.value

        # Get the minimal number of connectivities to use
        min_connectivities = self.minconnectivity.value
        self.connectivity_matrix[self.connectivity_matrix < min_connectivities] = 0

        # Create graph
        G, nodes = create_graph(self.connectivity_matrix, self.locations)

        # Plot graph
        if self.plottype.value == self.widgetsDict["graph_plot"]["options"]["plottype"]["values"][0]:
            plot_interactive_graph(G, connectivity_name)
        else:
            plotted = plot_static_graph(G, nodes, connectivity_name)
