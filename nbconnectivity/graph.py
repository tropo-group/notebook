import numpy as np
import networkx as nx
import matplotlib.pyplot as plt
import plotly.offline as py
import plotly.graph_objs as go

def create_graph(connectivity_matrix, locations):
    """
    Create a Networkx graph
    :param connectivity_matrix: a connectivity matrix as a Numpy array
    :param locations: starting or arrival locations as Pandas dataframe
    :return: graph and nodes
    """
    # Convert locations to a dictionary containing the longitude, latitude of each node as floats
    locations[['LONGITUDE', 'LATITUDE']] = locations[['LONGITUDE', 'LATITUDE']].astype(float)
    nodes_dict = locations[['LONGITUDE', 'LATITUDE']].values

    # Build a networkx graph
    G = nx.DiGraph(connectivity_matrix.values)

    # Attach node's positions to the graph
    nx.set_node_attributes(G, nodes_dict, 'pos')

    return G, nodes_dict


def plot_static_graph(G, nodes, connectivity_name):
    """

    :param G:
    :param nodes:
    :return: a boolean
    """
    # Create plot
    options = {
        'node_color': 'blue',
        'node_size': 10,
        'width': 1,
        'arrowstyle': '-|>',
        'arrowsize': 5,
        'edge_color': 'green'
    }

    plt.figure(figsize=(16, 10))
    nx.draw_networkx(G, nodes, arrows=True, with_labels=None, **options)
    plt.axis('off')
    plt.title(f'{connectivity_name} graph made with Tropolink')
    plotted = True

    return plotted

def plot_interactive_graph(G, connectivity_name):
    """

    :param G:
    :param connectivity_name:
    :return:
    """
    # Creates edges
    edge_x = []
    edge_y = []
    for edge in G.edges():
        x0, y0 = G.nodes[edge[0]]['pos']
        x1, y1 = G.nodes[edge[1]]['pos']
        edge_x.append(x0)
        edge_x.append(x1)
        edge_x.append(None)
        edge_y.append(y0)
        edge_y.append(y1)
        edge_y.append(None)

    edge_trace = go.Scatter(
        x=edge_x, y=edge_y,
        line=dict(width=0.8, color='#888'),
        hoverinfo='none',
        mode='lines')

    # Create nodes
    node_x = []
    node_y = []
    for node in G.nodes():
        x, y = G.nodes[node]['pos']
        node_x.append(x)
        node_y.append(y)

    node_trace = go.Scatter(
        x=node_x, y=node_y,
        mode='markers',
        hoverinfo='text',
        marker=dict(
            showscale=True,
            colorscale='YlGnBu',
            reversescale=False,
            color=[],
            size=10,
            colorbar=dict(
                thickness=15,
                title='Node Connections',
                xanchor='left',
                titleside='right'
            ),
            line_width=2))

    # Add a tooltip on nodes
    node_adjacencies = []
    node_text = []
    for node, adjacencies in enumerate(G.adjacency()):
        node_adjacencies.append(len(adjacencies[1]))
        node_text.append('# of connections: ' + str(len(adjacencies[1])))

    node_trace.marker.color = node_adjacencies
    node_trace.text = node_text

    # Create complete figure
    fig = go.Figure(data=[edge_trace, node_trace],
                    layout=go.Layout(
                        title=f'<br>{connectivity_name} graph made with Tropolink',
                        titlefont_size=16,
                        showlegend=False,
                        hovermode='closest',
                        margin=dict(b=20, l=5, r=5, t=40),
                        xaxis=dict(showgrid=False, zeroline=False, showticklabels=False),
                        yaxis=dict(showgrid=False, zeroline=False, showticklabels=False))
                    )
    fig.show()