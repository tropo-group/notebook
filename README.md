# INRA-Tropolink

[WIKI](https://gitlab.makina-corpus.net/inra-tropolink/project/-/wikis/home)

INRA Tropolink project : A web-tool to infer graphs of tropospheric connectivity from archived meteorological data.

## First clone

Add the ForgeMIA upstream:
```
git remote add forgemia git@forgemia.inra.fr:tropo-group/notebook.git
```
And don't forget to also push your code on it:
```
git push forgemia
```
## Run

Update the _tropolink_env_ Conda environment (if dependencies have changed):
```
sudo /opt/tljh/user/bin/conda env update -n tropolink_env --file tropolink_env.yml
```

Activate the conda environnement :
```
conda activate tropolink_env
```

Trust the notebook, so that it can be run automatically:
```
jupyter trust Connectivities\ Analysis.ipynb
```

Launch Jupyter :
```
jupyter notebook
```

Run all cells :

In the notebook menu, select Kernel > Restart and Run All.

## Quit
Close the conda environnement :
```
conda deactivate
```

## Developper's documentation 

[Tropolink Design documentation](tropolinkDesignDoc)

### API configuration

The urls used to get connectivities can be configured in the file `nbconnectivity/settings.py`.

## Admin documentation

To install TLJH or upgrade an existing installation, run:
```
# curl -L https://tljh.jupyter.org/bootstrap.py | python3 - --admin makina
```

To add new admin users from the CLI:
```
# useradd -m -u 21XXX -s /bin/bash -G jupyterhub-admins,jupyterhub-users jupyter-$USER 
# passwd jupyter-$USER
# tljh-config add-item users.admin makina
# tljh-config reload
```

### Deploiement

1. Se connecter au serveur du projet : 
`ssh -J rde@ssh-proxy tropolink3@staging-tropolink.fr`

2. `cd notebook` : dépôt git synchronisé sur forgemia et notre gitlab

3. `git status` pour vérifier qu'il n'y a pas de modif en local puis
`git pull` pour mettre à jour le contenu

4. Si besoin de mettre à jour le noyau, regénérer le noyau conda:

```
# sudo /opt/tljh/user/bin/conda env update -n tropolink_env --file tropolink_env.yml
```

5. Si besoin d'activer des extensions Jupyter requises :
```
# /opt/tljh/user/bin/pip install jupyter_contrib_nbextensions
# /opt/tljh/user/bin/jupyter contrib nbextension install --sys-prefix
# /opt/tljh/user/bin/jupyter nbextension enable init_cell/main --sys-prefix
# /opt/tljh/user/bin/jupyter nbextension enable hide_input/main --sys-prefix
```


